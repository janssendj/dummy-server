package org.wildfly.camel.examples.cdi;

import javax.enterprise.inject.Produces;
import javax.inject.Named;
import org.apache.camel.component.cxf.CxfEndpoint;
import org.apache.camel.component.cxf.DataFormat;
import org.apache.deltaspike.core.api.config.ConfigProperty;
import com.hypoworks.dummyservice.dummy.DummyPortType;

public class CxfEndpointConfig {
    
    @Produces()
    @Named("dummyService")
    public CxfEndpoint createCxfSoapEndpoint( @ConfigProperty(name = "wsdl.address") String publishedServerAddress) {
        CxfEndpoint cxfFromEndpoint = new CxfEndpoint();
        cxfFromEndpoint.setAddress(publishedServerAddress);
        cxfFromEndpoint.setServiceClass(DummyPortType.class);
        cxfFromEndpoint.setAddress("http://0.0.0.0:8080/DummyService");
        cxfFromEndpoint.setDataFormat(DataFormat.PAYLOAD);
        cxfFromEndpoint.setAllowStreaming(false);
        return cxfFromEndpoint;

    }
}
