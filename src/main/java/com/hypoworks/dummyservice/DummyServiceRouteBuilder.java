package com.hypoworks.dummyservice;

import org.apache.camel.ExchangePattern;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.properties.PropertiesComponent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import javax.enterprise.context.ApplicationScoped;
import org.apache.camel.cdi.ContextName;

@ApplicationScoped()
@ContextName("camel-cdi-context")
public class DummyServiceRouteBuilder extends RouteBuilder {

    public DummyServiceRouteBuilder() {
    }

    @Override
    public void configure() throws Exception {
        // normally custom ASB exceptionhandler here
        onException(Exception.class).handled(true);
        from("cxf:bean:dummyService").routeId("DummyService").log("${body}")
        .setExchangePattern(ExchangePattern.InOnly)
        //.to("amq:queue:FromDummyServiceQueue?timeToLive={{DummyService.timeToLive}}")
        .log("${body}")
        .processRef("dummyServiceResponseProcessor");
    }

    private void checkSecurityCertAvailable(String keystoreLocation, String keystorePassword) throws Exception {
        File f = new File(keystoreLocation);
        if (!f.exists()) {
            throw new FileNotFoundException("Keystore doesn't exist at the given location.");
        }
        try {
            KeyStore ks = KeyStore.getInstance("JKS");
            ks.load(new FileInputStream(keystoreLocation), keystorePassword.toCharArray());
        } catch (Exception e) {
            throw new KeyStoreException("Keystore loading error.");
        }
    }
}
