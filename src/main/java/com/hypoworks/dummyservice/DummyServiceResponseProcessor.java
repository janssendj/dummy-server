package com.hypoworks.dummyservice;

import com.hypoworks.dummyservice.dummy.response.DummyResponse;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@ApplicationScoped()
@Named()
public class DummyServiceResponseProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        DummyResponse dummyResponse = new DummyResponse();
        dummyResponse.setStatus("OK");
        exchange.getIn().setBody(dummyResponse);
    }
}
